//
//  FirebaseViewController.m
//  TestGPUImageFundationOC
//
//  Created by admin on 2020/1/14.
//  Copyright © 2020 AppTime. All rights reserved.
//

#import "FirebaseViewController.h"
#import <GPUImage/GPUImage.h>
#import <Firebase.h>
#import "TEGPUImageBigEyeFilter.h"

#define Width_Screen [[UIScreen mainScreen] bounds].size.width
#define Height_Screen [[UIScreen mainScreen] bounds].size.height

@interface FirebaseViewController () {
    float eyeVertexPoints[42 * 2];
    float eyeTexturePoits[42 * 2];
    
}

@property (nonatomic, strong) GPUImagePicture *imagePicture;
@property (nonatomic, strong) GPUImageView *imageView;
@property (nonatomic, strong) UISlider *slider;
@property (nonatomic, strong) UIButton *filterBtn;

@property (nonatomic, strong) GPUImageFilter *filter;

@property (nonatomic, strong) FIRVision *vision;

@property (nonatomic, strong) TEGPUImageBigEyeFilter *bigEyeFilter;

@end

@implementation FirebaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor yellowColor];
    
    [self.view addSubview:self.imageView];
    [self.view addSubview:self.slider];
    [self.view addSubview:self.filterBtn];
    
    _filter = [GPUImageFilter new];
    
    _bigEyeFilter = [TEGPUImageBigEyeFilter new];
    
    [self.imagePicture addTarget:_filter];
    [_filter addTarget:self.imageView];
    
    [self.imagePicture processImage];
    
    self.vision = [FIRVision vision];
}


#pragma mark -

- (GPUImagePicture *)imagePicture {
    if (!_imagePicture) {
        _imagePicture = [[GPUImagePicture alloc] initWithImage:[UIImage imageNamed:@"known.jpg"]];
        
    }
    
    return _imagePicture;
}

- (GPUImageView *)imageView {
    if (!_imageView) {
        _imageView = [[GPUImageView alloc] initWithFrame:CGRectMake(0, 0, Width_Screen, (Height_Screen/4*3))];
        _imageView.backgroundColor = [UIColor grayColor];
        
    }
    
    return _imageView;
}

- (UISlider *)slider {
    if (!_slider) {
        _slider = [[UISlider alloc] initWithFrame:CGRectMake(0, 0, Width_Screen - 80, 40)];
        _slider.center = CGPointMake(Width_Screen/2, (Height_Screen/4*3) + 30);
        _slider.maximumValue = 4;
        _slider.minimumValue = 0;
        [_slider addTarget:self action:@selector(clickChangeSliderWithEvent:) forControlEvents:UIControlEventValueChanged];
    }
    
    return _slider;
}

- (UIButton *)filterBtn {
    if (!_filterBtn) {
        _filterBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, Width_Screen / 2, 80)];
        _filterBtn.center = CGPointMake(Width_Screen / 2, Height_Screen - 60);
        _filterBtn.backgroundColor = [UIColor redColor];
        [_filterBtn setTitle:@"Big Eye" forState:UIControlStateNormal];
        [_filterBtn addTarget:self action:@selector(clickChangeFilterButtonWithEvent:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _filterBtn;
}

- (void)addPointWithIndex:(NSUInteger)index positionX:(NSNumber *)x positionY:(NSNumber *)y {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 2, 2)];
    label.center = CGPointMake(x.floatValue, y.floatValue);
    label.text = [NSString stringWithFormat:@"%lu", (unsigned long)index];
    label.backgroundColor = [UIColor brownColor];
    
    [self.view addSubview:label];
}


#pragma mark -

- (void)clickChangeFilterButtonWithEvent:(UIButton *)sender {
    FIRVisionFaceDetectorOptions *detectorOptions = [FIRVisionFaceDetectorOptions new];
    detectorOptions.landmarkMode = FIRVisionFaceDetectorLandmarkModeAll;
    detectorOptions.classificationMode = FIRVisionFaceDetectorClassificationModeAll;
    detectorOptions.performanceMode = FIRVisionFaceDetectorPerformanceModeAccurate;
    detectorOptions.contourMode = FIRVisionFaceDetectorContourModeAll;
    
    FIRVisionFaceDetector *faceDetector = [self.vision faceDetectorWithOptions:detectorOptions];
    
    FIRVisionImageMetadata *imageMetadata = [FIRVisionImageMetadata new];
    imageMetadata.orientation = FIRVisionDetectorImageOrientationTopLeft;
    
    FIRVisionImage *visionImage = [[FIRVisionImage alloc] initWithImage:[UIImage imageNamed:@"known.jpg"]];
    visionImage.metadata = imageMetadata;
    
    [faceDetector processImage:visionImage completion:^(NSArray<FIRVisionFace *> * _Nullable faces, NSError * _Nullable error) {
        FIRVisionFace *visionFace = [faces firstObject];
        
        [self setLeftEyeWithVisionFace:visionFace];
    }];
    
}

- (void)setLeftEyeWithVisionFace:(FIRVisionFace *)visionFace {
    
    //Left Eye
    FIRVisionFaceContour *leftEyeContour = [visionFace contourOfType:FIRFaceContourTypeLeftEye];
    NSArray<FIRVisionPoint *> *leftEyePoints = leftEyeContour.points;
    
    [leftEyePoints enumerateObjectsUsingBlock:^(FIRVisionPoint * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        //NSLog(@"index: %lu, X: %@ Y: %@", (unsigned long)idx, obj.x, obj.y);
        
        [self addPointWithIndex:idx positionX:obj.x positionY:obj.y];
    }];
    
    NSNumber *leftCenterNumberX = [NSNumber numberWithFloat:(leftEyePoints[4].x.floatValue + leftEyePoints[12].x.floatValue)/2];
    NSNumber *leftCenterNumberY = [NSNumber numberWithFloat:(leftEyePoints[4].y.floatValue + leftEyePoints[12].y.floatValue)/2];
    [self addPointWithIndex:17 positionX:leftCenterNumberX positionY:leftCenterNumberY];
    
    
    //Right Eye
    FIRVisionFaceContour *rightEyeContour = [visionFace contourOfType:FIRFaceContourTypeRightEye];
    NSArray<FIRVisionPoint *> *rightEyePoints = rightEyeContour.points;
    
    [rightEyePoints enumerateObjectsUsingBlock:^(FIRVisionPoint * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [self addPointWithIndex:idx positionX:obj.x positionY:obj.y];
    }];
    
    NSNumber *rightCenterNumberX = [NSNumber numberWithFloat:(rightEyePoints[4].x.floatValue + rightEyePoints[12].x.floatValue)/2];
    NSNumber *rightCenterNumberY = [NSNumber numberWithFloat:(rightEyePoints[4].y.floatValue + rightEyePoints[12].y.floatValue)/2];
    [self addPointWithIndex:17 positionX:rightCenterNumberX positionY:rightCenterNumberY];
    
    // 0 - 15
    for (int i = 0; i < leftEyePoints.count; i++) {
        FIRVisionPoint *visionPoint = leftEyePoints[i];
        
        eyeVertexPoints[i * 2] = [self changeToGLPointX:visionPoint.x.floatValue detectionWith:visionFace.frame.size.width];
        eyeVertexPoints[i * 2 + 1] = [self changeToGLPointY:visionPoint.y.floatValue detectionHeight:visionFace.frame.size.height];
    }
    
    //16 index
    eyeVertexPoints[16 * 2] = [self changeToGLPointX:leftCenterNumberX.floatValue detectionWith:visionFace.frame.size.width];
    eyeVertexPoints[16 * 2 + 1] = [self changeToGLPointY:leftCenterNumberY.floatValue detectionHeight:visionFace.frame.size.height];
    
    // 17 - 32
    for (int i = 17; i < leftEyePoints.count + 17; i++) {
        FIRVisionPoint *visionPoint = rightEyePoints[i - 17];
        
        eyeTexturePoits[i * 2] = [self changeToGLPointX:visionPoint.x.floatValue detectionWith:visionFace.frame.size.width];
        eyeTexturePoits[i * 2 + 1] = [self changeToGLPointY:visionPoint.y.floatValue detectionHeight:visionFace.frame.size.height];
    }
    
    // 33 index
    eyeVertexPoints[33 * 2] = [self changeToGLPointX:rightCenterNumberX.floatValue detectionWith:visionFace.frame.size.width];
    eyeVertexPoints[33 * 2 + 1] = [self changeToGLPointY:rightCenterNumberY.floatValue detectionHeight:visionFace.frame.size.height];
    
    NSLog(@"point 16: %f, point 33: %f", eyeVertexPoints[16], eyeVertexPoints[33]);
    
    //34 - 37 图像边沿坐标
    eyeVertexPoints[34 * 2] = 0;
    eyeVertexPoints[34 * 2 + 1] = 1;
    eyeVertexPoints[35 * 2] = 1;
    eyeVertexPoints[35 * 2 + 1] = 1;
    eyeVertexPoints[36 * 2] = 1;
    eyeVertexPoints[36 * 2 + 1] = 0;
    eyeVertexPoints[37 * 2] = 1;
    eyeVertexPoints[37 * 2 + 1] = -1;
    
    //38 - 42
    eyeVertexPoints[38 * 2] = - eyeVertexPoints[34 * 2];
    eyeVertexPoints[38 * 2 + 1] = -eyeVertexPoints[34 * 2 + 1];
    eyeVertexPoints[39 * 2] = -eyeVertexPoints[35 * 2];
    eyeVertexPoints[39 * 2 + 1] = -eyeVertexPoints[35 * 2 + 1];
    eyeVertexPoints[40 * 2] = -eyeVertexPoints[36 * 2];
    eyeVertexPoints[40 * 2 + 1] = -eyeVertexPoints[36 * 2 + 1];
    eyeVertexPoints[41 * 2] = -eyeVertexPoints[37 * 2];
    eyeVertexPoints[41 * 2 + 1] = -eyeVertexPoints[37 * 2 + 1];
    
    for (int i = 0; i < 42 * 2; i++) {
        eyeTexturePoits[i] = eyeVertexPoints[i] * 0.5 + 0.5;
    }
    
    //大眼滤镜
    [_bigEyeFilter setVertices:eyeVertexPoints texturePoints:eyeTexturePoits];
    
    [self.imagePicture removeAllTargets];
    [self.imagePicture addTarget:self.bigEyeFilter];
    [self.bigEyeFilter addTarget:self.imageView];
    [self.imagePicture processImage];
}

- (GLfloat)changeToGLPointX:(CGFloat)x detectionWith:(CGFloat)width {
    CGFloat widthImage = [UIImage imageNamed:@"known.jpg"].size.width;
    
    GLfloat tempX = (x - widthImage/2) / (widthImage / 2);
    
    return tempX;
}

- (GLfloat)changeToGLPointY:(CGFloat)y detectionHeight:(CGFloat)height {
    CGFloat heightImage = [UIImage imageNamed:@"known.jpg"].size.height;
    
    GLfloat tempY = (heightImage/2 - (heightImage - y)) / (heightImage/2);
    
    return tempY;
}

























- (void)clickChangeSliderWithEvent:(UISlider *)slider {
    //_customFilter.scale = slider.value;
    
    [self.imagePicture processImage];
}


@end
