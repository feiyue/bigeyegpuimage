//
//  GPUImageBigEyeFilter.h
//  TestGPUImageFundationOC
//
//  Created by admin on 2020/1/10.
//  Copyright © 2020 AppTime. All rights reserved.
//

#import <GPUImage/GPUImage.h>


@interface GPUImageBigEyeFilter : GPUImageFilter {
    GLint radiusUniform, centerUniform, scaleUniform;
}

@property (nonatomic, readwrite) CGPoint center;

@property (nonatomic, readwrite) CGFloat radius;

@property (nonatomic, readwrite) CGFloat scale;

@end

