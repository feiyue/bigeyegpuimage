//
//  GPUImageBigEyeFilter.m
//  TestGPUImageFundationOC
//
//  Created by admin on 2020/1/10.
//  Copyright © 2020 AppTime. All rights reserved.
//

#import "GPUImageBigEyeFilter.h"

#if TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE

NSString *const kGPUImageBigEyeFragmentShaderString = SHADER_STRING
(
 varying highp vec2 textureCoordinate;
 
 uniform sampler2D inputImageTexture;
 
 uniform highp vec2 center;
 uniform highp float radius;
 uniform highp float scale;
 
 void main()
 {
    highp vec2 textureCoordinateToUse = textureCoordinate;
    
    highp float dist = sqrt((center.x - textureCoordinateToUse.x) * (center.x - textureCoordinateToUse.x) + (center.y - textureCoordinateToUse.y) * (center.y - textureCoordinateToUse.y));
    
    if (dist < radius) {
        highp float ratio = sqrt(dist / radius);
        
        highp float x1 = center.x - (center.x - textureCoordinateToUse.x) * ratio * scale;
        highp float y1 = center.y - (center.y - textureCoordinateToUse.y) * ratio * scale;
        textureCoordinateToUse = vec2(x1, y1);
        
        //color
        lowp vec4 textureColor = texture2D(inputImageTexture, textureCoordinateToUse);
        
        gl_FragColor = vec4((textureColor.rgb + vec3(0.4)), textureColor.w);
        //texture2D(inputImageTexture, textureCoordinateToUse);
    }else {
        
        gl_FragColor = texture2D(inputImageTexture, textureCoordinate);
    }
    
 }
);

#else

NSString *const kGPUImageBigEyeFragmentShaderString = SHADER_STRING
(
 varying highp vec2 textureCoordinate;
 
 uniform sampler2D inputImageTexture;
 
 uniform highp vec2 center;
 uniform highp float radius;
 uniform highp float scale;
 
 void main()
 {
    highp vec2 textureCoordinateToUse = textureCoordinate;
    
    highp float dist = sqrt((center.x - textureCoordinateToUse.x) * (center.x - textureCoordinateToUse.x) + (center.y - textureCoordinateToUse.y) * (center.y - textureCoordinateToUse.y));
    
    if (dist < radius) {
        highp float ratio = sqrt(dist / radius);
        
        highp float x1 = center.x - (center.x - textureCoordinateToUse.x) * ratio * scale;
        highp float y1 = center.y - (center.y - textureCoordinateToUse.y) * ratio * scale;
        textureCoordinateToUse = vec2(x1, y1);
        
        //color
        lowp vec4 textureColor = texture2D(inputImageTexture, textureCoordinateToUse);
        gl_FragColor = vec4((textureColor.rgb + vec3(0.4)), textureColor.w);
        
    }else {
        
        gl_FragColor = texture2D(inputImageTexture, textureCoordinate);
    }
    
 }
);

#endif

@implementation GPUImageBigEyeFilter

@synthesize center = _center;
@synthesize radius = _radius;
@synthesize scale = _scale;

- (id)init {
    if (!(self = [super initWithFragmentShaderFromString:kGPUImageBigEyeFragmentShaderString])) {
        return nil;
    }
    
    radiusUniform = [filterProgram uniformIndex:@"radius"];
    centerUniform = [filterProgram uniformIndex:@"center"];
    scaleUniform = [filterProgram uniformIndex:@"scale"];
    
    self.radius = 0.03;
    self.center = CGPointMake(0.43, 0.2);
    self.scale = 1.2;
    
    return self;
}

- (void)setCenter:(CGPoint)center {
    _center = center;
    
    CGPoint rotatePoint = [self rotatedPoint:_center forRotation:inputRotation];
    [self setPoint:rotatePoint forUniform:centerUniform program:filterProgram];
}

- (void)setRadius:(CGFloat)radius {
    _radius = radius;
    
    [self setFloat:_radius forUniform:radiusUniform program:filterProgram];
}

- (void)setScale:(CGFloat)scale {
    _scale = scale;
    
    [self setFloat:_scale forUniform:scaleUniform program:filterProgram];
}


@end
//不使用默认的顶点//默认的vertices: {-1.0, -1.0, 1.0, -1.0, -1.0, 1.0, 1.0, 1.0}
//glVertexAttribPointer(filterPositionAttribute, 2, GL_FLOAT, 0, 0, vertices);
//GLfloat meshVertices[8] = {1.0, -1.0, -1.0, -1.0, 1.0, 1.0, -1.0, 1.0};
//glVertexAttribPointer(filterPositionAttribute, 2, GL_FLOAT, 0, 0, &meshVertices[0]);
