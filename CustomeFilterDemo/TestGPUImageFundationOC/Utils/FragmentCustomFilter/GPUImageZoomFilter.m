//
//  GPUImageZoomFilter.m
//  TestGPUImageFundationOC
//
//  Created by admin on 2020/1/10.
//  Copyright © 2020 AppTime. All rights reserved.
//

#import "GPUImageZoomFilter.h"

#if TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE

NSString *const kGPUImageZoomFragmentShaderString = SHADER_STRING
(
 varying highp vec2 textureCoordinate;
 
 uniform sampler2D inputImageTexture;
 
 void main()
 {
    //缩放 0.5
    //highp vec2 textureCoordinateToUse = vec2(textureCoordinate.x * 2.0 - 0.5, textureCoordinate.y * 2.0 - 0.5);
    
    //平行
    highp vec2 textureCoordinateToUse = vec2(textureCoordinate.x * 2.0 + textureCoordinate.y - 1.0, textureCoordinate.y);
    
    gl_FragColor = texture2D(inputImageTexture, textureCoordinateToUse);
 }
 );

#else

NSString *const kGPUImageZoomFragmentShaderString = SHADER_STRING
(
 varying highp vec2 textureCoordinate;
 
 uniform sampler2D inputImageTexture;
 
 void main()
 {
    //highp vec2 textureCoordinateToUse = vec2(textureCoordinate.x * 2.0 - 0.5, textureCoordinate.y * 2.0 - 0.5);
    
    //平行
    highp vec2 textureCoordinateToUse = vec2(textureCoordinate.x * 2.0 + textureCoordinate.y - 1.0, textureCoordinate.y);
    
    gl_FragColor = texture2D(inputImageTexture, textureCoordinateToUse);
 }
 );

#endif

@implementation GPUImageZoomFilter

- (id)init {
    if (!(self = [super initWithFragmentShaderFromString:kGPUImageZoomFragmentShaderString])) {
        return nil;
    }
    
    return self;
}

@end
