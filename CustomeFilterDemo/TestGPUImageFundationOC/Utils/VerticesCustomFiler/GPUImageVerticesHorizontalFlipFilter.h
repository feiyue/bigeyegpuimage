//
//  GPUImageVerticesHorizontalFlipFilter.h
//  TestGPUImageFundationOC
//
//  Created by admin on 2020/1/10.
//  Copyright © 2020 AppTime. All rights reserved.
//

#import <GPUImage/GPUImage.h>

@interface GPUImageVerticesHorizontalFlipFilter : GPUImageFilter {
    
}

- (void)setVertices:(const GLfloat *)vertices;

@end

