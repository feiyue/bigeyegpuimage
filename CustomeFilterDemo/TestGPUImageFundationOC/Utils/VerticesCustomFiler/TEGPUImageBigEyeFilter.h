//
//  GPUImageBigEyeFilter.h
//  TestGPUImageFundationOC
//
//  Created by admin on 2020/1/14.
//  Copyright © 2020 AppTime. All rights reserved.
//

#import <GPUImage/GPUImage.h>


@interface TEGPUImageBigEyeFilter : GPUImageFilter

- (void)setVertices:(const GLfloat *)vertices texturePoints:(const GLfloat *)textures;

@end

