//
//  GPUImageBigEyeFilter.m
//  TestGPUImageFundationOC
//
//  Created by admin on 2020/1/14.
//  Copyright © 2020 AppTime. All rights reserved.
//

#import "TEGPUImageBigEyeFilter.h"
#import "TEFaceBaseData.h"

NSString *const kMKGPUImagePlasticFragmentShaderString1 = SHADER_STRING
(
 
#ifdef GL_FRAGMENT_PRECISION_HIGH
 precision highp float;
#else
 precision mediump float;
#endif
 
 varying highp vec2 textureCoordinate;
 
 uniform sampler2D inputImageTexture;
 
 // 图像笛卡尔坐标系的关键点，也就是纹理坐标乘以宽高得到
 uniform highp vec2 cartesianPoints[34];
 
 // 纹理宽度
 uniform int textureWidth;
 // 纹理高度
 uniform int textureHeight;
 
 // 是否允许美型处理，存在人脸时为1，没有人脸时为0
 uniform int enableReshape;
 
 // 大眼处理
vec2 enlargeEye(lowp vec2 currentCoordinate, vec2 circleCenter, float radius, float intensity)
{
    float currentDistance = distance(currentCoordinate, circleCenter);
    float weight = currentDistance / radius;
    weight = 1.0 - intensity * (1.0 - weight * weight);
    weight = clamp(weight, 0.0, 1.0);
    currentCoordinate = circleCenter + (currentCoordinate - circleCenter) * weight;
    
    return currentCoordinate;
}
 
 void main()
 {
     vec2 coordinate = textureCoordinate.xy;
     if (enableReshape == 0 || (cartesianPoints[16].x / float(textureWidth) <= 0.03) || (cartesianPoints[16].y / float(textureHeight) <= 0.03)) {
        gl_FragColor = texture2D(inputImageTexture, coordinate);
     }else {

         // 将坐标转成图像大小，这里是为了方便计算
         coordinate = textureCoordinate * vec2(float(textureWidth),float(textureHeight));

         // 两个瞳孔的距离
         lowp float eyeDistance = distance(cartesianPoints[16], cartesianPoints[33]);

         // 大眼
         float eyeEnlarge = 0.8 * 0.25; // 放大倍数
         if (eyeEnlarge > 0.0) {
             lowp float radius = eyeDistance * 0.33; // 眼睛放大半径
             coordinate = enlargeEye(coordinate, cartesianPoints[16] + (cartesianPoints[33] - cartesianPoints[16]) * 0.05, radius, eyeEnlarge);
             coordinate = enlargeEye(coordinate, cartesianPoints[33] + (cartesianPoints[16] - cartesianPoints[33]) * 0.05, radius, eyeEnlarge);
         }

         // 转变回原来的纹理坐标系
         coordinate = coordinate / vec2(float(textureWidth), float(textureHeight));
         gl_FragColor = texture2D(inputImageTexture, coordinate);
     }
 }
 );

@interface TEGPUImageBigEyeFilter () {
    GLint _cartesianPointsSlot;
    GLint _textureWidthSlot;
    GLint _textureHeighSlot;
    GLint _enableReshapeSlot;

    // 顶点坐标
    float vertexPoints[42 * 2];
    // 纹理坐标
    float texturePoints[42 * 2];

    // 笛卡尔坐标系
    float cartesianVertices[34 * 2];
    
}

@end

@implementation TEGPUImageBigEyeFilter

- (void)setVertices:(const GLfloat *)vertices texturePoints:(const GLfloat *)textures {
    for (int i = 0; i < (42 * 2); i++) {
        vertexPoints[i] = vertices[i];
        texturePoints[i] = textures[i];
    }
    
}


- (id)initWithVertexShaderFromString:(NSString *)vertexShaderString fragmentShaderFromString:(NSString *)fragmentShaderString {
    
    if (!(self = [super initWithVertexShaderFromString:vertexShaderString fragmentShaderFromString:kMKGPUImagePlasticFragmentShaderString1])) {
        return nil;
    }

    _cartesianPointsSlot = [filterProgram uniformIndex:@"cartesianPoints"];
    _textureWidthSlot = [filterProgram uniformIndex:@"textureWidth"];
    _textureHeighSlot = [filterProgram uniformIndex:@"textureHeight"];
    _enableReshapeSlot = [filterProgram uniformIndex:@"enableReshape"];

    return self;
    
}


- (void)renderToTextureWithVertices:(const GLfloat *)vertices textureCoordinates:(const GLfloat *)textureCoordinates;{
    
    if (self.preventRendering) {
        [firstInputFramebuffer unlock];
        return;
    }
    
    [GPUImageContext setActiveShaderProgram:filterProgram];
    
    outputFramebuffer = [[GPUImageContext sharedFramebufferCache] fetchFramebufferForSize:[self sizeOfFBO] textureOptions:self.outputTextureOptions onlyTexture:NO];
    [outputFramebuffer activateFramebuffer];
    
    if (usingNextFrameForImageCapture) {
        [outputFramebuffer lock];
    }
    
    [self setUniformsForProgramAtIndex:0];
    
    glClearColor(0, 0, 0, 0);
    glClear(GL_COLOR_BUFFER_BIT);
    
    // 如果存在人脸数据则绘制美型效果，否则绘制原图
    //if (MKLandmarkManager.shareManager.faceData && MKLandmarkManager.shareManager.faceData.count > 0) {
        
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, [firstInputFramebuffer texture]);
        glUniform1i(filterInputTextureUniform, 2);
        
        int length = sizeof(vertexPoints)/sizeof(vertexPoints[0]);
        
        //[MKLandmarkEngine.shareManager generateFaceAdjustVertexPoints:vertexPoints withTexturePoints:texturePoints withLength:length withFaceIndex:0];
        
        glVertexAttribPointer(filterPositionAttribute, 2, GL_FLOAT, 0, 0, vertexPoints);
        glVertexAttribPointer(filterTextureCoordinateAttribute, 2, GL_FLOAT, 0, 0, texturePoints);
        
        glUniform1i(_textureWidthSlot, [firstInputFramebuffer size].width);
        glUniform1i(_textureHeighSlot, [firstInputFramebuffer size].height);
        
        
        for (int i = 0; i < 34; i ++) {
            cartesianVertices[i * 2] = texturePoints[i * 2] * [firstInputFramebuffer size].width;
            cartesianVertices[i * 2 + 1] = texturePoints[i * 2 + 1] * [firstInputFramebuffer size].height;
        }
        
        glUniform2fv(_cartesianPointsSlot, 34, cartesianVertices);
        
        glUniform1i(_enableReshapeSlot, 1);
        
        glDrawElements(GL_TRIANGLES,sizeof(FaceImageIndices)/sizeof(FaceImageIndices[0]), GL_UNSIGNED_SHORT, FaceImageIndices);
//    } else {
//        glActiveTexture(GL_TEXTURE2);
//        glBindTexture(GL_TEXTURE_2D, [firstInputFramebuffer texture]);
//
//        glUniform1i(filterInputTextureUniform, 2);
//
//        glVertexAttribPointer(filterPositionAttribute, 2, GL_FLOAT, 0, 0, vertices);
//        glVertexAttribPointer(filterTextureCoordinateAttribute, 2, GL_FLOAT, 0, 0, textureCoordinates);
//
//        glUniform1i(_enableReshapeSlot, 0);
//        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
//    }
//
    [firstInputFramebuffer unlock];
}

@end
