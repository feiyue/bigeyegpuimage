//
//  ImageDemoViewController.m
//  MagicCamera
//
//  Created by admin on 2020/1/13.
//  Copyright © 2020 黎宁康. All rights reserved.
//

#import "ImageDemoViewController.h"
#import <GPUImage/GPUImage.h>
#import "MKGPUImagePlasticFilter.h"
#import "GPUImageBigEyeFilter.h"

#import "MKGPUImageTrackOutput.h"
#import "MKLandmarkManager.h"

//
#import "MKGPUImagePicture.h"
#import "MKGPUImageContext.h"
#import "MKGPUImageView.h"
#import "MKEffectFilter.h"
#import "MKGPUImageFilter.h"

#import "MKEffectHandler.h"
#import "MKLandmarkEngine.h"

#define Width_Screen [[UIScreen mainScreen] bounds].size.width
#define Height_Screen [[UIScreen mainScreen] bounds].size.height


@interface ImageDemoViewController () {
    // 顶点坐标
    float vertexPoints[122 * 2];
    // 纹理坐标
    float texturePoints[122 * 2];
}

@property (nonatomic, strong) GPUImagePicture *imagePicture;
@property (nonatomic, strong) GPUImageView *imageView;
@property (nonatomic, strong) UISlider *slider;
@property (nonatomic, strong) UIButton *filterBtn;

@property (nonatomic, strong) GPUImageFilter *filter;
@property (nonatomic, strong) GPUImageBigEyeFilter *plasticFilter;

@property (nonatomic, strong) MKEffectFilter *effectFilter;

//----
//@property (nonatomic, strong) MKGPUImagePicture *imagePicture;
//@property (nonatomic, strong) MKGPUImageView *imageView;


//@property (nonatomic, strong) MKGPUImageFilter *filter;
//@property (nonatomic, strong) MKGPUImagePlasticFilter *plasticFilter;

//
@property (nonatomic, strong) MKGPUImageTrackOutput *trackOutput;

@property (nonatomic, strong) NSTimer *timer;

@end

@implementation ImageDemoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _effectFilter = [[MKEffectFilter alloc] init];
    
    self.view.backgroundColor = [UIColor yellowColor];
    
    [self.view addSubview:self.imageView];
    [self.view addSubview:self.slider];
    [self.view addSubview:self.filterBtn];
    
    self.plasticFilter = [[GPUImageBigEyeFilter alloc] init];
    
    self.filter = [GPUImageFilter new];
    [self.imagePicture addTarget:self.filter];
    [self.filter addTarget:self.imageView];
    
    [self.imagePicture processImage];
    
    MKGPUImageContext *context = [[MKGPUImageContext alloc] initWithCurrentGLContext];
    self.trackOutput = [[MKGPUImageTrackOutput alloc] initWithContext:context];
    
    self.timer = [NSTimer timerWithTimeInterval:4.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
        if (!MKLandmarkManager.shareManager.faceData &&  MKLandmarkManager.shareManager.faceData.count == 0) {
            [self.trackOutput detectImage:[UIImage imageNamed:@"known.jpg"]];
            
        }
        
        int length = sizeof(vertexPoints)/sizeof(vertexPoints[0]);
        [MKLandmarkEngine.shareManager generateFaceAdjustVertexPoints:vertexPoints withTexturePoints:texturePoints withLength:length withFaceIndex:0];
        
        NSLog(@"face count: %lu", (unsigned long)MKLandmarkManager.shareManager.faceData.count);
        
        NSLog(@"point 74: %f, point 77: %f", vertexPoints[74], vertexPoints[77]);
        
        NSLog(@"face ");
    }];
    
    [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
}

#pragma mark -

- (void)clickChangeFilterButtonWithEvent:(UIButton *)sender {
    [self.imagePicture removeAllTargets];
    
    [self.imagePicture addTarget:self.plasticFilter];
    [self.plasticFilter addTarget:self.imageView];
    [self.imagePicture processImage];
}

- (void)clickChangeSliderWithEvent:(UISlider *)slider {
    //_customFilter.scale = slider.value;
    
    [self.imagePicture processImage];
}


#pragma mark -

- (GPUImagePicture *)imagePicture {
    if (!_imagePicture) {
        //MKGPUImageContext *context = [[GPUImagePicture alloc] initWithCurrentGLContext];
        _imagePicture = [[GPUImagePicture alloc] initWithImage:[UIImage imageNamed:@"known.jpg"]];
        
    }
    
    return _imagePicture;
}

- (GPUImageView *)imageView {
    if (!_imageView) {
        _imageView = [[GPUImageView alloc] initWithFrame:CGRectMake(0, 0, Width_Screen, (Height_Screen/4*3))];
        _imageView.backgroundColor = [UIColor grayColor];
        
    }
    
    return _imageView;
}

- (UISlider *)slider {
    if (!_slider) {
        _slider = [[UISlider alloc] initWithFrame:CGRectMake(0, 0, Width_Screen - 80, 40)];
        _slider.center = CGPointMake(Width_Screen/2, (Height_Screen/4*3) + 30);
        _slider.maximumValue = 4;
        _slider.minimumValue = 0;
        [_slider addTarget:self action:@selector(clickChangeSliderWithEvent:) forControlEvents:UIControlEventValueChanged];
    }
    
    return _slider;
}

- (UIButton *)filterBtn {
    if (!_filterBtn) {
        _filterBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, Width_Screen / 2, 80)];
        _filterBtn.center = CGPointMake(Width_Screen / 2, Height_Screen - 60);
        _filterBtn.backgroundColor = [UIColor redColor];
        [_filterBtn setTitle:@"Change Filter" forState:UIControlStateNormal];
        [_filterBtn addTarget:self action:@selector(clickChangeFilterButtonWithEvent:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _filterBtn;
}


@end
